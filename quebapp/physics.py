#!/usr/bin/env python3
# -*- coding: utf-8 -*-

'''
This module include all the physics (and more) related to the system
dynamics.
'''


# Author: Mauro Faccin 2014
# -------------------------
# |   This is QuEBApp     |
# -------------------------
# |    License: GPL3      |
# |   see LICENSE.txt     |
# -------------------------

from __future__ import print_function, division
import numpy as np
import itertools,collections
import string
import copy

NORM = False
TOL = 1e-10
VERB = False
AZ = list(map(str,range(10)))+list(string.ascii_uppercase)

class System():
    def __init__(self,H,init=None,weight=None):
        if is_hermitian(H):
            self.Hamiltonian = H
        else:
            raise TypeError('Not an Hamiltonian')

        if init is None:
            self.rho0 = np.ones_like(H, dtype=complex)/len(H)
        else:
            self.rho0 = np.copy(init, dtype=complex)
        self._spectrum = False
        self._ave_dm = False
        self.C = {}
    def spectral_decomposition(self):
        if self._spectrum:
            return self.eigvals, self.eigproj
        else:
            l,v = spectral_decomposition(self.Hamiltonian)
            self.eigvals = l
            self.eigproj = v
            self.spectrum = True
            return self.eigvals, self.eigproj
    def gap(self):
        _,_ = self.spectral_decomposition()
        ls = np.unique(self.eigvals)
        return ls[1] - ls[0]
    def average_density_matrix(self,T='infty'):
        if self._ave_dm:
            return self.ave_dm
        else:
            self.ave_dm = average_density_matrix(self,T=T)
            self._ave_dm = True
            return self.ave_dm
    def node_closeness(self,distance='fidelity',T='infty'):
        if (distance,T) not in self.C:
            if distance in ['transport']:
                if T == 'zero':
                    H = np.abs(self.Hamiltonian)
                    _C = (H**2)/3
                else:
                    MM = average_transport(self,T=T)
                    _C = 2.0 * np.abs(MM)
            elif distance in ['purity']:
                if T == 'zero':
                    _C = short_purity(self)
                else:
                    _C = average_purity(self,T=T)
            elif distance in ['fidelity']:
                if T == 'zero':
                    _C = short_fidelity(self)
                else:
                    F2 = average_fidelity(self,T=T)
                    _C = np.real(F2)
                    if NORM:
                        diag  = np.abs(np.diag(self.average_density_matrix())*np.diag(self.rho0))
                        diag  = np.sqrt(diag)
                        _C /= np.outer(diag,diag)
            elif distance in ['fidelity_phases']:
                if T == 'zero':
                    H = self.Hamiltonian
                    diag = np.diag(H)
                    diag2 = np.resize(np.diag(np.dot(H,H)),[len(H),len(H)])
                    _C = np.outer(diag,diag)-0.5*(diag2+diag2.transpose())
                else:
                    F2 = average_fidelity_phases(self,T=T)
                    _C = np.abs(F2)
            elif distance in ['purity_phases']:
                if T == 'zero':
                    _C = -4 * np.abs(self.Hamiltonian**2) / 3
                else:
                    _C = average_purity_phases(self,T=T)
            else:
                raise TypeError('Distance "{0}" not defined'.format(d))
            _C -= np.diag(np.diag(_C))
            self.C[(distance,T)]=_C
        return self.C[(distance,T)]
    def __len__ (self):
        return len(self.Hamiltonian)

class Link():
    def __init__(self,i,j):
        # new cluster
        self.start = i
        # list of merging clusters
        self.end = j

class Dendrogram (list):
    def __init__(self,N):
        list.__init__(self)
        self.append(Partition(N))
        self.best = 0
        self.best_community = Partition(N)
    def merge(self, list_of_merges, closeness, mod):
        new_partition = self[-1].merge(list_of_merges,closeness,mod)
        self.append(new_partition)
    def best_partition(self):
        self.best = len(self)-1
        self.best_community = self[-1]

class Partition ():
    def __init__(self,N,part=None,closeness=1.0,mod=0.0,links=None):
        if part is None:
            self.partition = [[n] for n in range(N)]
        else:
            self.partition = part
        self.N = N
        self.closeness = closeness
        self.modularity = mod
        self.links = links
    def __len__(self):
        return len(self.list())
    def merge (self, list_of_merges, closeness=1.0, mod=0.0):
        new_partition = self._unshared_copy(self.partition)
        indxes = list(range(len(self.partition)))
        for to_merge in list_of_merges:
            for indx in to_merge[1:]:
                new_partition[to_merge[0]].extend(self.partition[indx])
        # remove in reverse order the unwonted clusters...
        for indx in sorted([p for to_merge in list_of_merges for p in\
                            to_merge[1:]],reverse=True):
            new_partition.pop(indx)
            indxes.pop(indx)
        
        # this point from actual indexing to the new one
        indxes = dict(zip(indxes,list(range(len(indxes)))))
        merged = [indxes[to_merge[0]] for to_merge in list_of_merges]
        links = []
        for i,j in indxes.items():
            if j not in merged:
                links.append(Link(j,[i]))
            else:
                indx = merged.index(j)
                links.append(Link(j,list_of_merges[indx]))

        return Partition(self.N,new_partition,\
                         closeness=closeness,\
                         mod=mod,\
                         links=links)
    def _unshared_copy(self,inList):
        # remove internal references in the list of lists
        if isinstance(inList, list):
            return list( map(self._unshared_copy, inList) )
        return inList
    def matrix(self):
        Nc = len(self.partition)
        M = np.zeros([self.N,Nc])
        for n,p in enumerate(self.partition):
            M[p,n] = 1
        return M
    def list(self):
        pair = [(n,pp) for n,p in enumerate(self.partition) for pp in p]
        l = [i for i,j in sorted(pair,key=lambda x:x[1])]
        return l
    def __str__(self):
        l = [AZ[ll] if ll < len(AZ) else '*' for ll in self.list()]
        return ''.join(l)
    def __repr__(self):
        l = [AZ[ll] if ll < len(AZ) else '*' for ll in self.list()]
        return ''.join(l)

def average_fidelity_phases(system,T='infty'):
    '''
    Compute average fidelity averaged over time and initial phases.

    Args:
        system (System): initial system (System).

    Kwargs:
        T (string): upper time for averafe (string of float).

    Returns:
        Closeness matrix (numpy.array)

    '''
    N   = len(system)
    L,P = system.spectral_decomposition()
    
    # compute the matrix
    M = np.zeros_like(system.Hamiltonian, dtype=complex)
    for Pk in P:
        M += np.outer(np.diag(Pk),np.diag(Pk))
    # if Upper limit of integration is given, integrate up to T
    if T != 'infty':
        if isinstance(T,collections.Iterable):
            M0 = np.copy(M)
            M = []
            for time in T:
                if time == 0.0:
                    M.append(system.rho0)
                else:
                    Mt = np.copy(M0)
                    for lk,Pk in zip(L,P):
                        for lq,Pq in zip(L,P):
                            if lq!=lk:
                                phase = (np.exp(1j*(lk-lq)*time)-1.0)/(1j*(lk-lq)*time)
                                Mt += np.outer(np.diag(Pk),np.diag(Pq))*phase
                    M.append(Mt)
        else:
            if T == 0.0:
                M = Pinit
            else:
                for lk,Pk in zip(L,P):
                    for lq,Pq in zip(L,P):
                        if lq!=lk:
                            phase = (np.exp(1j*(lk-lq)*T)-1.0)/(1j*(lk-lq)*T)
                            M += np.outer(np.diag(Pk),np.diag(Pq))*phase
    return M/N**2

def average_purity_phases(system,T='infty'):
    N = len(system)
    L,P = system.spectral_decomposition()
    
    # compute the matrix
    M = np.ones_like(system.Hamiltonian, dtype=complex)
    for k,Pk in enumerate(P):
        Pk2 = np.abs(Pk)**2
        for l,Pl in enumerate(P):
            Pl2 = np.abs(Pl)**2
            M -= np.dot(Pk2,Pl2.T)
            if l != k:
                M -= np.dot(Pk*Pl.T,Pk*Pl.T)
    # if Upper limit of integration is given, integrate up to T
    if T != 'infty':
        if isinstance(T,collections.Iterable):
            M0 = np.copy(M)
            M = []
            for time in T:
                if time == 0.0:
                    M.append(system.rho0)
                else:
                    Mt = np.copy(M0)
                    for lk,Pk in zip(L,P):
                        for lq,Pq in zip(L,P):
                            if lq!=lk:
                                phase = (np.exp(1j*(lk-lq)*time)-1.0)/(1j*(lk-lq)*time)
                                Mt += np.outer(np.diag(Pk),np.diag(Pq))*phase
                    M.append(Mt)
        else:
            if T == 0.0:
                M = Pinit
            else:
                for lk,Pk in zip(L,P):
                    for lq,Pq in zip(L,P):
                        if lq!=lk:
                            phase = (np.exp(1j*(lk-lq)*T)-1.0)/(1j*(lk-lq)*T)
                            M += np.outer(np.diag(Pk),np.diag(Pq))*phase
    return np.abs(M)/N**2

def average_unitary(system, T='infty'):
    Nn = len(system.Hamiltonian)
    L,P = system.spectral_decomposition()
    
    # compute the matrix
    M = np.zeros_like(system.Hamiltonian, dtype=complex)
    for Pk in P:
        M += np.abs(Pk)**2
    # if Upper limit of integration is given, integrate up to T
    if T != 'infty':
        if isinstance(T,collections.Iterable):
            M0 = np.copy(M)
            M = []
            for time in T:
                if time == 0.0:
                    M.append(system.rho0)
                else:
                    Mt = np.copy(M0)
                    for lk,Pk in zip(L,P):
                        for lq,Pq in zip(L,P):
                            if lq!=lk:
                                phase = (np.exp(1j*(lk-lq)*time)-1.0)/(1j*(lk-lq)*time)
                                Mt += (Pk*Pq)*phase
                    M.append(Mt)
        else:
            if T == 0.0:
                M = system.rho0
            else:
                for lk,Pk in zip(L,P):
                    for lq,Pq in zip(L,P):
                        if lq!=lk:
                            phase = (np.exp(1j*(lk-lq)*T)-1.0)/(1j*(lk-lq)*T)
                            M += (Pk*Pq)*phase
    return np.sqrt(np.outer(np.diag(M),np.diag(M)))

def average_fidelity(system, T='infty'):
    DM = system.average_density_matrix(T=T)
    return DM.T * system.rho0

def average_purity(system, T='infty'):
    '''TODO: rewrite finite time T
    '''
    Nn = len(system)
    L,P = system.spectral_decomposition()
    
    # compute the matrix
    M = 2 * np.abs(average_density_matrix(system,T=T))**2
    for k,Pk in enumerate(P):
        Pk_rho = np.dot(Pk,system.rho0)
        for q,Pq in enumerate(P):
            if k!=q:
                Pk_rho_Pq = np.dot(Pk_rho,Pq)
                M += 2 * np.abs(Pk_rho_Pq)**2
    # if Upper limit of integration is given, integrate up to T
    if T != 'infty':
        raise NotImplementedError('Still ToDo')
        if isinstance(T,collections.Iterable):
            M0 = np.copy(M)
            M = []
            for time in T:
                if time == 0.0:
                    M.append(system.rho0)
                else:
                    Mt = np.copy(M0)
                    for lk,Pk in zip(L,P):
                        for lq,Pq in zip(L,P):
                            if lq!=lk:
                                phase = (np.exp(1j*(lk-lq)*time)-1.0)/(1j*(lk-lq)*time)
                                Mt += (Pk*Pq)*phase
                    M.append(Mt)
        else:
            if T == 0.0:
                M = system.rho0
            else:
                for lk,Pk in zip(L,P):
                    for lq,Pq in zip(L,P):
                        if lq!=lk:
                            phase = (np.exp(1j*(lk-lq)*T)-1.0)/(1j*(lk-lq)*T)
                            M += (Pk*Pq)*phase
    #return np.sqrt(np.outer(np.diag(M),np.diag(M)))
    return M

def average_density_matrix(system, T='infty'):
    '''
    Compute the time average of the system avolution in the class system
    
    Input:
        system:  Hamiltonian/network + initial state
        T (optional): end time for the average (if None then infinity)
    Output:
        <rho>: time average of the state
    '''
    
    Nn = len(system.Hamiltonian)
    L,P = system.spectral_decomposition()
    
    # compute the matrix
    M = np.zeros_like(system.Hamiltonian, dtype=complex)
    for Pk in P:
        M += dots(Pk,system.rho0,Pk)
    # if Upper limit of integration is given, integrate up to T
    if T != 'infty':
        if isinstance(T,collections.Iterable):
            M0 = np.copy(M)
            M = []
            for time in T:
                if time == 0.0:
                    M.append(system.rho0)
                else:
                    Mt = np.copy(M0)
                    for lk,Pk in zip(L,P):
                        for lq,Pq in zip(L,P):
                            if lq!=lk:
                                phase = (np.exp(1j*(lk-lq)*time)-1.0)/(1j*(lk-lq)*time)
                                Mt += dots(Pk,system.rho0,Pq)*phase
                    M.append(Mt)
        else:
            if T == 0.0:
                M = system.rho0
            else:
                for lk,Pk in zip(L,P):
                    for lq,Pq in zip(L,P):
                        if lq!=lk:
                            phase = (np.exp(1j*(lk-lq)*T)-1.0)/(1j*(lk-lq)*T)
                            M += dots(Pk,system.rho0,Pq)*phase
    return M

def average_transport(system,T='infty',symmetric=True):
    '''
    Time average of the probability of going from :math:`i` to :math:`j`.
    
    Input:
        system: Hamiltonian or network + initial state
        T (optional): end time for the average (if None then infinity -> the
        mixing matrix is returned)
        symmetric(optional): whether the matrix should be symmetrized
    
    Output:
        MM: time average of the transport (mixing matrix as t -> infty)
    '''
    M=np.zeros_like(system.Hamiltonian)
    L,P = system.spectral_decomposition()
    
    for p in P:
        M += np.abs(p * p)
    if T != 'infty':
        if isinstance(T,collections.Iterable):
            M0 = np.copy(M)
            M = []
            for time in T:
                if time == 0.0:
                    M.append(np.abs(H)**2)
                else:
                    Mt = np.copy(M0)
                    for l1,p1 in zip(L,P):
                        for l2,p2 in zip(L,P):
                            if l1 > l2:
                                Mt += 2 * np.sin((l1-l2) * time) * (p1 * p2)/((l1-l2)*time)
                    M.append(Mt)
        else:
            if T == 0.0:
                M = np.abs(H)**2
            else:
                for l1,p1 in zip(L,P):
                    for l2,p2 in zip(L,P):
                        if l1 > l2:
                            Mt += 2 * np.sin((l1-l2) * T) * (p1 * p2)/((l1-l2)*T)
    if symmetric and not is_symmetric(M):
        M = (M+M.T)/2.0
    return M

def merge_clusters(C, partition, list_of_merges):
    ''' compute the average closeness between clusters upon cluster merging
    '''
    # node-wise weights
    w = np.ones(len(partition))/len(partition)
    # cluster-wise weights
    ww = [np.sum(w[c]) for c in partition.partition]
    new_partition = partition.merge(list_of_merges)
    # new cluster-wise weights
    new_ww = [np.sum(w[c]) for c in new_partition.partition]
    ww,new_ww = map(np.array,[ww,new_ww])
    mnew = dict([(end,l.start) for l in new_partition.links for end in l.end])
    
    # multiply by its weight and sum on the first column
    for to_merge in list_of_merges:
        for ii in to_merge:
            _ww = ww * ww[ii]
            C[ii,:] *= _ww
            C[:,ii] *= _ww
        C[to_merge[0],:] = np.sum(C[to_merge,:],axis=0)
        C[:,to_merge[0]] = np.sum(C[:,to_merge],axis=1)

    # suppress one of the two community
    toremove = sorted([i for l in list_of_merges for i in l[1:]],reverse=True)
    for ii in toremove:
        C=np.delete(C,ii,axis=0)
        C=np.delete(C,ii,axis=1)

    # nodes that need normalization
    tonormalize = sorted([mnew[l[0]] for l in list_of_merges],reverse=True)
    # Normalize it
    for n,tonorm in enumerate(tonormalize):
        _ww = new_ww * new_ww[tonorm]
        for ii in tonormalize[:n]:
            _ww[ii] = 1.0
        C[tonorm,:] /= _ww
        C[:,tonorm] /= _ww

    # suppress the diagonal
    C -= np.diag(np.diag((C)))
    return C, new_partition

def find_new_clusters(C,\
                      perm_inv=True,\
                      tol=1e-10):
    '''turn a list of lists of clusters with the same minimum distance min(D)
    '''
    Nc = len(C)

    def find_cl(i,cls):
        for n,c in enumerate(cls):
            if i in c:
                return n
        return None

    itriu = np.triu_indices(Nc,1) # upper triangular indices
    val_max = np.max(C[itriu])  # minimum val
    indx_max = np.where(np.abs(C[itriu]-val_max) < tol) 
    if perm_inv:
        # if permutation invariant approach: join all the pairs at the same
        # distance
        imaxs = []
        for indx in indx_max[0]:
            ii = itriu[0][indx]
            jj = itriu[1][indx]

            ci = find_cl(ii,imaxs)
            cj = find_cl(jj,imaxs)

            if ci is None:
                if cj is None:
                    imaxs.append([ii,jj])
                else:
                    imaxs[cj].append(ii)
            elif cj is None:
                imaxs[ci].append(jj)
            else:
                if ci != cj:
                    imaxs[ci].extend(imaxs[cj])
                    imaxs.pop(cj)
    else:
        # if not using the permutation invariant approach: choose a random
        # pair
        raise NotImplemented('This is not implemented: permutation affected algorithm')
        rand_el = np.random.randint(len(indx_max[0]))
        imaxs = [[ itriu[0][rand_el], itriu[1][rand_el] ]]
    return imaxs,val_max

def perturb_closeness(C,tol=TOL):
    N = len(C)
    # flatten the closeness (upper part only)
    diffs = np.sort(C[np.triu_indices(N,1)], axis=None)
    # get close differences
    diffs = np.array([d - diffs[n-1] for n,d in enumerate(diffs[1:])])
    small_diff = diffs[ np.argmax(diffs>tol) ]
    rg_state = np.random.get_state()
    np.random.seed(1235547664)
    if small_diff < tol:
        pert = 0.5 * np.random.randn(N,N)  * 1e-3
    else:
        pert = 0.5 * np.random.randn(N,N) * small_diff * 1e-3
    np.random.set_state(rg_state)    
    return np.copy(C) + pert + pert.T

def find_communities_from_closeness(C,\
                     tol=TOL, perturb=False,\
                     perm_inv=True\
                    ):
    N=len(C)
    if perturb:
        CC = perturb_closeness(C,tol=tol)
    else:
        CC = np.copy(C)
    
    modmax = 0
    dendrogram = Dendrogram(N)
    partition = Partition(N)
    while len(CC)>1:
        imaxs,val_max = find_new_clusters(CC,perm_inv=perm_inv)
        # update distances and clusters
        CC, partition = merge_clusters(CC,partition,imaxs)
        # compute modularity and check for maximum
        mod = modularity(C,partition.matrix(),neg=True,safe_diag=True)
        dendrogram.merge(imaxs,val_max,mod)
        if VERB:
            print ('{0:10.4g} <= Cij <= {1:<10.4g}'.format(np.min(CC),np.max(CC)),end='')
            print ('{0:< 15.8g} {1:< 15.8g} {2}'.format(qm,val_max,imaxs))
        if mod > modmax:
            modmax = mod
            dendrogram.best_partition()
    return dendrogram

def find_communities(system,distance='fidelity',T='infty',\
                     tol=TOL, perturb=False,\
                     perm_inv=True\
                    ):
    C = system.node_closeness(distance,T=T)
    return find_communities_from_closeness(C,tol=tol,\
                                           perturb=perturb,\
                                           perm_inv=perm_inv
                                          )

def purity(rho):
    return np.sum(np.abs(rho)**2)

def short_fidelity(system):
    Nn = len(system)
    H =system.Hamiltonian
    rho0 = system.rho0

    comm = np.dot(H,rho0)-np.dot(rho0,H)
    first = -2 * (comm*rho0.T)
    if np.sum(first.imag) < TOL:
        H2 = np.dot(H,H)
        anti_comm = np.dot(H2,rho0)+np.dot(rho0,H2)
        second = (2*dots(H,rho0,H) - anti_comm) * rho0.T
        if VERB:
            print ('fidelity: second order')
        return second.real
    else:
        if VERB:
            print ('fidelity: first order')
        return first.imag

def short_purity(system):
    H = system.Hamiltonian
    rho0 = system.rho0

    Hrho = np.dot(H,rho0)
    rhoH = np.dot(rho0,H)
    comm = Hrho-rhoH 

    if is_symmetric(H) and is_symmetric(rho0):
        HrhoH = np.dot(Hrho,H)
        second = 2 * (2*HrhoH*rho0.T - np.abs(comm)**2)
        anticomm = np.dot(H,Hrho) + np.dot(rhoH,H)
        second += 2 * anticomm*rho0.T
        if VERB:
            print ('purity: second order')
        return second.real
    else:
        first = -4 * (rho0*comm.T)
        if VERB:
            print ('purity: first order')
        return first.imag

def dots(*args):
    '''Compute matrix multiplication of args
    '''
    if len(args) > 0:
        A = np.eye(len(args[0]))
        for a in args:
            A = np.dot(A,a)
        return A
    else:
        return 0

def spectral_decomposition(A,tol=TOL):
        r"""Spectral decomposition of a Hermitian matrix.

        Returns the unique eigenvalues a and the corresponding projectors P
        for the Hermitian matrix A, such that :math:`A = \sum_k  a_k P_k`.
        """
        # Ville Bergholm 2010

        d, v = eighsort(A)
        d = d.real  # A is assumed Hermitian

        # combine projectors for degenerate eigenvalues
        a = [d[0]]
        P = [projector(v[:, 0])]
        for k in range(1,len(d)):
                temp = projector(v[:, k])
                if abs(d[k] - d[k-1]) > tol:
                        # new eigenvalue, new projector
                        a.append(d[k])
                        P.append(temp)
                else:
                        # same eigenvalue, extend current P
                        P[-1] += temp
        return np.asarray(a), P

def eighsort(A):
        """Returns eigenvalues and eigenvectors of a Hermitian matrix, sorted in nonincreasing order."""
        d, v = np.linalg.eigh(A)
        ind = d.argsort()[::-1]  # nonincreasing real part
        return d[ind], v[:, ind]

def projector(v):
        """Projector corresponding to vector v."""
        return np.outer(v, v.conj())

def is_symmetric(M,tol=TOL):
    delta = np.sum(np.abs(M-M.T))
    if np.abs(delta) < tol:
        return True
    else:
        return False

def is_hermitian(M,tol=TOL):
    delta = np.sum(np.abs(M-M.T.conj()))
    if np.abs(delta) < tol:
        return True
    else:
        return False

def null_hypothesis(A,safe_diag=False):
    '''
    compute the null hypothesis for the modularity
    '''
    if safe_diag and not is_symmetric(A):
        raise RuntimeError('Save diagonal only works for symmetric\
                           matrices!!!')
    if safe_diag:
        A_diag = np.diag(A)
        loc_A = A - np.diag(A_diag)
    else:
        loc_A = np.copy(A)
    m = np.sum(loc_A)
    if m > 0.:
        k_in = np.sum(loc_A,axis=0)
        k_out = np.sum(loc_A,axis=1)
        null_A = np.outer(k_in,k_out)/m
        if safe_diag:
            null_diag = np.diag(null_A)
            null_A -= np.diag(null_diag)
            null_A *= m/(m - np.sum(null_diag))
    else:
        return np.zeros_like(A)
    if safe_diag:
        return null_A + np.diag(A_diag)
    else:
        return null_A

def modularity(A,partition,neg=True,safe_diag=False):
    '''
    Return the modularity of the given partition and graph

    Input:
        G: Graph
        partition: partition (list)
    Output:
        Modularity
    @article{PhysRevE.69.026113,
        title = {Finding and evaluating community structure in networks},
        author = {Newman, M. E. J. and Girvan, M.},
        journal = {Phys. Rev. E},
        volume = {69},
        issue = {2},
        pages = {026113},
        numpages = {15},
        year = {2004},
        month = {Feb},
        doi = {10.1103/PhysRevE.69.026113},
        url = {http://link.aps.org/doi/10.1103/PhysRevE.69.026113},
        publisher = {American Physical Society}}
    '''
    #A = nx.to_numpy_matrix(G,weight=weight)
    if type(partition) == list:
        H = utils.part_list_to_matrix (partition)
        N = len(A)
        Nc = len(set(partition)) # number of partition
    else:
        H = partition
        N,Nc = H.shape

    if neg and np.sum(A[A<0]) != 0:
        lm=lp=1
        null_A = np.zeros_like(A)

        # Positive part of A
        Ap = A.copy()
        Ap[A<0] = 0.
        null_A += lp * null_hypothesis(Ap,safe_diag=safe_diag)
        m_p = np.sum(Ap)
        
        #Negative Part of A
        Am = -A.copy()
        Am[Am<0] = 0.
        null_A -= lm * null_hypothesis(Am,safe_diag=safe_diag)
        m_m = np.sum(Am)

        # the following only care about normalizatin of modularity (we
        # don't care about positive and negative links).
        m = m_m + m_p
    else:
        m = np.sum(A)
        null_A = null_hypothesis(A,safe_diag=safe_diag)

    return np.trace(np.dot(np.dot(H.T,A-null_A),H))/m
