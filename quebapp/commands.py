# Author: Mauro Faccin 2014
# -------------------------
# |   This is QuEBApp     |
# -------------------------
# |    License: GPL3      |
# |   see LICENSE.txt     |
# -------------------------
from __future__ import print_function, division
from quebapp import physics

def print_best_community(dendrogram,filename='q_output.txt'):
    """Print best community to screen and file

    Input:
        :dendrogram: quebapp.Dendrogram; results
        :filename: string, file name
    Output:
        :nothing:

    :returns: nothing

    """
    with open(filename,'w') as fout:
        b_com = dendrogram[dendrogram.best]
        print('best community:  ',b_com,file=fout)
        print('best modularity: ',b_com.modularity,file=fout)
    print(dendrogram.best_community)

def print_dendrogram(dendro,filename='q_dendrogram.txt',\
                     logscale = True,\
                     linsteps = False):
    """write dendrogram to file

    Input:
        :dendro: quebapp.Dendrogram, results
        :filename: string, file name to write

    Output:
        :returns: nothing

    """
    '''Write dendrogram to file to be plotted with gnuplot (or other programs)'''
    import pylab
    import matplotlib.cm as cm
    
    Nnodes = len(dendro[0])
    maxclo = dendro[0].closeness
    Nlevel = len(dendro)
    best_partition = dendro.best_community.list()
    Nc = max(best_partition)+1

    with open(filename,'w') as fout:
        print ('communities    modularity',file=fout)
        for lev in dendro:
            print (lev,lev.modularity,file=fout)
    
    #find and ordering such that no link cross
    order = []
    def ordering(level,node,o):
        links = dendro[level].links
        # links starting from node "node"
        link = [l for l in links if l.start==node][0]
        for end in link.end:
            if level==1:
                o.append(end)
            else:
                o = ordering(level-1,end,o)
        return o
    order = ordering (Nlevel-1,0,order)
    
    class Position():
        def __init__ (self,position=0,\
                      closeness=1.0,\
                      last=False,\
                      level=0,\
                      node=0,\
                      links=None,\
                      com=0):
            self.npos = position
            self.pos = position/(Nnodes-1)
            self.clo = closeness
            self.last = last
            self.lev = level
            self.com = com
            self.links=links
            self.node=node

    #add node position
    pos={}
    for n,node in enumerate(order):
        com = best_partition[node]
        pos[(0,node)] = Position(position=n,\
                                 closeness=dendro[0].closeness,\
                                 node=node,\
                                 com=com)
    for level,l in enumerate(dendro):
        clo,clusters,links = l.closeness,l.list(),l.links
        if level > 0:
            for link in links:
                if len(link.end)>1:
                    npos = [pos[(level-1,leaf)].npos for leaf in link.end]
                    ncom = [pos[(level-1,leaf)].com for leaf in link.end]
                    if len(set(ncom))==1:
                        ncom = ncom[0]
                    else:
                        ncom = Nc
                    pos[(level,link.start)] = Position(position=(max(npos)+min(npos))/2.0,\
                                                       closeness=clo,\
                                                       last=True,\
                                                       level=level,\
                                                       com=ncom,\
                                                       links=link.end)
                else: # no leafs for this node
                    nn = pos[(level-1,link.end[0])]
                    pos[(level,link.start)] = Position(position=nn.npos,\
                                                       closeness=nn.clo,\
                                                       com=nn.com,\
                                                       level=nn.lev)

    # Draw the dendrogram
    pylab.figure()
    if logscale:
        pylab.xscale('log',nonposx='clip')
    if linsteps:
        pylab.xlabel('Steps')
    else:
        pylab.gca().invert_xaxis()
        pylab.xlabel('Closeness')
    pylab.ylabel('Index')
    pylab.ylim(-0.05, 1.05)
    pylab.yticks([n/(Nnodes-1) for n in range(Nnodes)],order)
    for cross in pos:
        level,node = cross
        p = pos[cross]
        if p.last:
            leafs = [pos[(level-1,leaf)] for leaf in p.links]
            color = cm.Set1(p.com/Nc) if p.com < Nc else 'k'
            # plot horizontal lines
            for l in leafs:
                if linsteps:
                    xs = [l.lev,p.lev]
                else:
                    xs = [l.clo,p.clo]
                ys = [l.pos,l.pos]
                pylab.plot(xs,ys,lw=2.0,c=color)
            # plot vertical line (joining horizontal ones)
            if linsteps:
                xs = [p.lev,p.lev]
            else:
                xs = [p.clo,p.clo]
            ys = [min([l.pos for l in leafs]), max([l.pos for l in leafs])]
            pylab.plot(xs,ys,lw=2.0,c=color)


    pylab.show()

    return order

def communitize (system, distance='fidelity', T='infty',\
                  dendro=False, perm_inv=True\
                 ):
    '''Compute the communities of the given quantum system

    Input:
        :system: quebapp.System, from the given Hamiltonian
        :distance: string, which distance to use
                   ['fidelity','transport','purity']
        :T: string, time window ['infty','zero']
        :dendro: bool, true if dendrogram is requested
        :perm_inv: bool, use the permutation invariant algorithm

    Output:
        - communities: quebapp.Dendrogram, communities results
    '''
    # actual closness measure
    C = system.node_closeness(distance,T=T)
    normalization = lambda x,y: x*y

    # get the communities (and the dendrogram)
    communities = find_communities(C,normalization=normalization,\
                          dendro=dendro,\
                          perturb=perturb,\
                          perm_inv=perm_inv\
                         )
    return communities



