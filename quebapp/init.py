# Author: Mauro Faccin 2014
# -------------------------
# |   This is QuEBApp     |
# -------------------------
# |    License: GPL3      |
# |   see LICENSE.txt     |
# -------------------------
import sys
import os
import argparse
import string
import numpy as np

import quebapp
#from quebapp.physics import System,Dendrogram,find_communities
from quebapp import commands,physics

TOL = 1e-10
AZ = list(map(str,range(10)))+list(string.ascii_uppercase)

def read_edgelist(filename):
    try:
        edges = {}
        nodes = set()
        with open(filename,'r') as fin:
            for line in fin:
                l = line.split()
                if len(l) == 2: l.append(1)
                i,j,w = l
                edges[(i,j)] = complex(w)
                nodes.add(i)
                nodes.add(j)
        nodemap = dict(zip(sorted(nodes,key=int),range(len(nodes))))
    except:
        raise TypeError('Unreadable file: {}'.format(filename))

    # write to screen the dictionary used:
    print ('WARNING: The following dictionary has been used')
    print ('{:>15s} -> {:<10s}'.format('node-name','node-index'))
    for k,v in nodemap.items():
        print ('{:>15s} -> {:<10d}'.format(k,v))
    
    N = len(nodes)
    H = np.zeros([N,N],dtype=complex)

    for i,j in edges:
        w = edges[(i,j)]
        H[nodemap[i],nodemap[j]] = w
        if i!=j:
            H[nodemap[j],nodemap[i]] = np.conjugate(w)
    return H

def main():
    # Parse command line arguments
    parser = argparse.ArgumentParser(description='QuEBApp: Quantum\
                                     Community Detection' )
    parser.add_argument('--edgelist', action='store_true',\
                        help='Read Hamiltonian as list of edges [i j\
                        weight(optional)]')
    parser.add_argument('network',\
                        help='Network file [default=Hamiltonian matrix]')
    parser.add_argument('-0','--time0',action='store_true',\
                        help='Closeness measure with short-time limit\
                        [default: long-time limit]')
    parser.add_argument('-t','--type',type=str,choices=['fidelity','transport','purity'],\
                        default='fidelity',\
                        help='Use a specific closeness measure\
                        [default: fidelity]')
    parser.add_argument('-d','--dendrogram',action='store_true',\
                        help='Outputs the resulting dendrogram\
                        and modularity')
    args = parser.parse_args()

    # read the Hamiltonian
    if args.edgelist:
        H = read_edgelist(args.network)
    else:
        try:
            H = np.loadtxt(args.network)
        except:
            raise TypeError('Unreadable file: {}'.format(args.network))

    system = physics.System (H)

    # 
    T = 'zero' if args.time0 else 'infty'

    # find communities
    dendrogram = physics.find_communities(system,distance=args.type,T=T)

    if args.dendrogram:
        commands.print_best_community(dendrogram)
        commands.print_dendrogram(dendrogram)
    else:
        commands.print_best_community(dendrogram)
        



