
Command line usage
==================

This code is both a Python_ module and provides a standalone command for
the terminal.

.. _Python: http://www.python.org/

Arguments
---------

Usage:
        quebapp [-h] [--edgelist] [-0] [-t TYPE] [-d] network

Positional arguments:
        network     
               Network file [default=Hamiltonian matrix]

Optional arguments:
        -h, --help         show this help message and exit
        --edgelist         Read Hamiltonian as list of edges [i j weight(optional)]
        -0, --time0        Closeness measure with short-time limit [default: long-time limit]
        -t, --type TYPE    Use a specific closeness measure [default: fidelity]
        -d, --dendrogram   Outputs the resulting dendrogram and modularity

TYPE can be either `fidelity`, `transport` or `purity`

Input file
----------

The network representing the quantum system needs to be a Hermitian matrix
(the Hamiltonian). It can be given in matrix form:

.. math::
        &h_{11}\ & h_{12}\ & h_{13}\ & ...\\
        &h_{21}\ & h_{22}\ & h_{23}\ & ...\\
        &h_{31}\ & h_{32}\ & h_{33}\ & ...\\
        &...\ &  ...\  & ...\ &  ...

with :math:`h_{ij}` complex conjugate of :math:`h_{ji}`.
The matrix can be given as an edge list:

.. math::
        & i\  &  j\  & h_{ij}\\
        &\dots\ & \dots\ & \dots\\

where :math:`i` and :math:`j` are indexes and :math:`h_{ij}` 
the Hamiltonian entry (:math:`h_{ji}` will be computed automatically).

