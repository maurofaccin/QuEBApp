.. QuEBApp documentation master file, created by
   sphinx-quickstart2 on Tue Nov  4 11:21:54 2014.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

QuEBApp's documentation!
========================

This is a `python` module to compute quantum communities based on
publication:

`Faccin M et al. Community Detection in Quantum Complex Networks, PRX
(2014) <http://journals.aps.org/prx/abstract/10.1103/PhysRevX.4.041012>`_

.. toctree::
   :maxdepth: 2
   :numbered:
   
   usage/index
   module/index


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

