#!/usr/bin/env python3
# Author: Mauro Faccin 2014
# -------------------------
# |   This is QuEBApp     |
# -------------------------
# |    License: GPL3      |
# |   see LICENSE.txt     |
# -------------------------

try:
    from setuptools import setup
except ImportError:
    from distutils.core import setup
import quebapp


setup(name='quebapp',
      version=quebapp.__version__,
      description=quebapp.__description__,
      long_description = quebapp.__long_description__, 
      author=quebapp.__author__,
      author_email=quebapp.__author_email__,
      url=quebapp.__url__,
      license=quebapp.__copyright__,
      packages=['quebapp'],
      scripts=['bin/quebapp'],
      requires=[
          'argparse (>=2.7)',
          'numpy',
          'scipy',
          'subprocess (>=2.7)'],
      provides='quebapp',
      classifiers      = [
        'Development Status :: 4 - Beta',
        'Environment :: Console',
        'Intended Audience :: End Users/Desktop',
        'Intended Audience :: Science/Research',
        'Intended Audience :: Information Technology',
        'License :: OSI Approved :: GNU General Public License v3 (GPLv3)',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Topic :: Scientific/Engineering :: Physics'
        ],
)
